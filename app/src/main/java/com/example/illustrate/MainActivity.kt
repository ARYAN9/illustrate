package com.example.illustrate

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.text.style.BackgroundColorSpan
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import top.defaults.colorpicker.ColorPickerPopup

class MainActivity : AppCompatActivity() {
    private lateinit var drawingView: DrawingBoardView

    companion object{
        private const val STORAGE_PERMISSION_CODE = 1
        private const val GALLERY = 100
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        drawingView = findViewById<DrawingBoardView>(R.id.drawingView)

        drawingView.setBrushSize(8.0F)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK) {
            if(requestCode == GALLERY) {
                try {
                    if(data!!.data != null) {
                        val imageBackground = findViewById<ImageView>(R.id.imgBackground)
                        imageBackground.setImageURI(data!!.data)
                    } else {
                        Toast.makeText(
                                this,
                                "Error parsing the image!",
                                Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    public fun onShowBrushDialog(view: View){

        val brushDialog = Dialog(this)
        brushDialog.setContentView(R.layout.dialog_brush)
        brushDialog.setTitle("Select Brush Size:")

        brushDialog.show()

        val smallBrushBtn = brushDialog.findViewById<ImageButton>(R.id.btnSmallBrush)
        smallBrushBtn.setOnClickListener{
            drawingView.setBrushSize(4.0F);
            brushDialog.dismiss()
        }
        val mediumBrushBtn = brushDialog.findViewById<ImageButton>(R.id.btnMediumBrush)
        mediumBrushBtn.setOnClickListener{
            drawingView.setBrushSize(8.0F);
            brushDialog.dismiss()
        }

        val largeBrushBtn = brushDialog.findViewById<ImageButton>(R.id.btnLargeBrush)
        largeBrushBtn.setOnClickListener{
            drawingView.setBrushSize(15.0F);
            brushDialog.dismiss()
        }
    }
    public fun onShowColorPicker(view: View) {
        ColorPickerPopup.Builder(this)
                .initialColor(Color.RED) // Set initial color
                .enableBrightness(true) // Enable brightness slider or not
                .enableAlpha(true) // Enable alpha slider or not
                .okTitle("Choose")
                .cancelTitle("Cancel")
                .showIndicator(true)
                .showValue(true)
                .build()
                .show(view, object : ColorPickerPopup.ColorPickerObserver() {
                    override fun onColorPicked(color: Int) {
                        drawingView.setColor(color)
                    }

                    fun onColor(color: Int, fromUser: Boolean) {}
                })
    }

    public fun openGallery(view: View){
        if(isReadStorageAllowed()){
            //open the gallery
            val pickImageIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(pickImageIntent,GALLERY)
            //read the image
            //set the background
        }else{
            requestStoragePermission()
        }
    }

    public fun onUndo(view: View){
        drawingView.undo()
    }

    public fun onRedo(view: View){
        drawingView.redo()
    }

    private fun requestStoragePermission(){
        ActivityCompat.requestPermissions(this,
                arrayOf(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                STORAGE_PERMISSION_CODE
        )
    }

    private fun isReadStorageAllowed(): Boolean{
        return ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    }

    private fun isWriteStorageAllowed(): Boolean{
        return ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    }
}